package ru.kitaev.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.kitaev.tm")
public class ApplicationConfiguration {
    
}
