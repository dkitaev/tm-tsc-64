package ru.tsc.kitaev.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.listener.AbstractListener;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public class FileScanner implements Runnable {

    @NotNull
    private static final String PATH = "./";

    private static final int INTERVAL = 10;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    @Autowired
    public Bootstrap bootstrap;

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    public void init() {
        for (@NotNull final AbstractListener listener : listeners) {
            commands.add(listener.command());
        }
        es.scheduleWithFixedDelay(this::run, INTERVAL, INTERVAL, TimeUnit.SECONDS);
    }

    public void run() {
        @NotNull final File file = new File(PATH);
        Arrays.stream(file.listFiles())
                .filter(f -> f.isFile() && commands.contains(f.getName()))
                .forEach(f -> {
                    @NotNull final String name = f.getName();
                    try {
                        bootstrap.runCommand(name);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        f.delete();
                    }
                });
    }

}
