package ru.tsc.kitaev.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.kitaev.tm.api.service.ISessionService;
import ru.tsc.kitaev.tm.endpoint.SessionDTO;

@Getter
@Setter
@Service
public class SessionService implements ISessionService {

    @Nullable
    private SessionDTO session;

}
