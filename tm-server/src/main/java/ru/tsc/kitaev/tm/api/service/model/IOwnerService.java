package ru.tsc.kitaev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.AbstractOwnerModel;

import java.util.List;

public interface IOwnerService<E extends AbstractOwnerModel> extends IService<E> {

    void clear(@Nullable final String userId);

    @NotNull
    List<E> findAll(@Nullable final String userId);

    @NotNull
    List<E> findAll(@Nullable final String userId, @Nullable final String sort);

    @Nullable
    E findById(@Nullable final String userId, @Nullable final String id);

    @NotNull
    E findByIndex(@Nullable final String userId, @Nullable final Integer index);

    void removeById(@Nullable final String userId, @Nullable final String id);

    void removeByIndex(@Nullable final String userId, @Nullable final Integer index);

    boolean existsById(@Nullable final String userId, @Nullable final String id);

    boolean existsByIndex(@Nullable final String userId, final int index);

    @NotNull
    Integer getSize(@Nullable final String userId);

}
