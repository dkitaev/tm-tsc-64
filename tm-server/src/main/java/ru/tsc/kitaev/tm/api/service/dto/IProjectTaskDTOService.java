package ru.tsc.kitaev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.dto.TaskDTO;

import java.util.List;

public interface IProjectTaskDTOService {

    @NotNull
    List<TaskDTO> findTaskByProjectId(@Nullable String userId, @Nullable String projectId);

    void bindTaskById(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void unbindTaskById(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void removeById(@Nullable String userId, @Nullable String projectId);

    void removeByIndex(@Nullable String userId, @Nullable Integer index);

    void removeByName(@Nullable String userId, @Nullable String name);

}
