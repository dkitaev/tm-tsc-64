package ru.tsc.kitaev.tm.log;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class OperationEvent {

    private OperationType type;

    private Object entity;

    private String table;

    private Long timestamp = System.currentTimeMillis();

    public OperationEvent(OperationType type, Object entity) {
        this.type = type;
        this.entity = entity;
    }

}
