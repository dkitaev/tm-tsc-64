package ru.tsc.kitaev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.kitaev.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.kitaev.tm.api.service.dto.ITaskDTOService;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.exception.AbstractException;
import ru.tsc.kitaev.tm.dto.SessionDTO;
import ru.tsc.kitaev.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@WebService
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @WebMethod
    @Override
    public void clearTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) throws AbstractException {
        sessionService.validate(session);
        taskService.clear(session.getUserId());
    }

    @WebMethod
    @Override
    @NotNull
    public List<TaskDTO> findAllTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) throws AbstractException {
        sessionService.validate(session);
        return taskService.findAll(session.getUserId());
    }

    @WebMethod
    @Override
    @NotNull
    public List<TaskDTO> findAllTaskSorted(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "sort", partName = "sort") @Nullable final String sort
    ) throws AbstractException {
        sessionService.validate(session);
        return taskService.findAll(session.getUserId(), sort);
    }

    @WebMethod
    @Override
    @Nullable
    public TaskDTO findTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        sessionService.validate(session);
        return taskService.findById(session.getUserId(), id);
    }

    @WebMethod
    @Override
    @NotNull
    public TaskDTO findTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        sessionService.validate(session);
        return taskService.findByIndex(session.getUserId(), index);
    }

    @WebMethod
    @Override
    public void removeTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        sessionService.validate(session);
        taskService.removeById(session.getUserId(), id);
    }

    @WebMethod
    @Override
    public void removeTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        sessionService.validate(session);
        taskService.removeByIndex(session.getUserId(), index);
    }

    @WebMethod
    @Override
    @NotNull
    public TaskDTO createTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) throws AbstractException {
        sessionService.validate(session);
        return taskService.create(session.getUserId(), name, description);
    }

    @WebMethod
    @Override
    @NotNull
    public TaskDTO findTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        sessionService.validate(session);
        return taskService.findByName(session.getUserId(), name);
    }

    @WebMethod
    @Override
    public void removeTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        sessionService.validate(session);
        taskService.removeByName(session.getUserId(), name);
    }

    @WebMethod
    @Override
    public void updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AbstractException {
        sessionService.validate(session);
        taskService.updateById(session.getUserId(), id, name, description);
    }

    @WebMethod
    @Override
    public void updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AbstractException {
        sessionService.validate(session);
        taskService.updateByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    @Override
    public void startTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        sessionService.validate(session);
        taskService.startById(session.getUserId(), id);
    }

    @WebMethod
    @Override
    public void startTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        sessionService.validate(session);
        taskService.startByIndex(session.getUserId(), index);
    }

    @WebMethod
    @Override
    public void startTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        sessionService.validate(session);
        taskService.startByName(session.getUserId(), name);
    }

    @WebMethod
    @Override
    public void finishTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        sessionService.validate(session);
        taskService.finishById(session.getUserId(), id);
    }

    @WebMethod
    @Override
    public void finishTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        sessionService.validate(session);
        taskService.finishByIndex(session.getUserId(), index);
    }

    @WebMethod
    @Override
    public void finishTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        sessionService.validate(session);
        taskService.finishByName(session.getUserId(), name);
    }

    @WebMethod
    @Override
    public void changeTaskStatusById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) throws AbstractException {
        sessionService.validate(session);
        taskService.changeStatusById(session.getUserId(), id, status);
    }

    @WebMethod
    @Override
    public void changeTaskStatusByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) throws AbstractException {
        sessionService.validate(session);
        taskService.changeStatusByIndex(session.getUserId(), index, status);
    }

    @WebMethod
    @Override
    public void changeTaskStatusByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) throws AbstractException {
        sessionService.validate(session);
        taskService.changeStatusByName(session.getUserId(), name, status);
    }

    @Override
    @WebMethod
    public boolean existsTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        sessionService.validate(session);
        return taskService.existsById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public boolean existsTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") final int index
    ) throws AbstractException {
        sessionService.validate(session);
        return taskService.existsByIndex(session.getUserId(), index);
    }

}
